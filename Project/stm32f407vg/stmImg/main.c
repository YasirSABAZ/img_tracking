#include "stm32f4xx.h"                  // Device header
#include "stm32f4xx_usart.h"            // Keil::Device:StdPeriph Drivers:USART
#include "stm32f4xx_gpio.h"             // Keil::Device:StdPeriph Drivers:GPIO
#include "stm32f4xx_rcc.h"              // Keil::Device:StdPeriph Drivers:RCC
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void led_config(void)
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
	
	GPIO_InitTypeDef GPIOD_ledConfig;
	GPIOD_ledConfig.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_3 | GPIO_Pin_5 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_10 ;
	GPIOD_ledConfig.GPIO_Mode = GPIO_Mode_OUT;
	GPIOD_ledConfig.GPIO_OType = GPIO_OType_PP;
	GPIOD_ledConfig.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIOD_ledConfig.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIOD_ledConfig);
	
	GPIO_InitTypeDef GPIOE_ledConfig;
	GPIOE_ledConfig.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_12 | GPIO_Pin_14 ;
	GPIOE_ledConfig.GPIO_Mode = GPIO_Mode_OUT;
	GPIOE_ledConfig.GPIO_OType = GPIO_OType_PP;
	GPIOE_ledConfig.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIOE_ledConfig.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOE, &GPIOE_ledConfig);
}

void stepperConfig(void)
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	
	GPIO_InitTypeDef GPIO_Stepper;
	GPIO_Stepper.GPIO_Mode	= GPIO_Mode_OUT;
	GPIO_Stepper.GPIO_Pin		= GPIO_Pin_1 | GPIO_Pin_3 | GPIO_Pin_5 | GPIO_Pin_2;
	GPIO_Stepper.GPIO_Speed	=	GPIO_Speed_50MHz;
	GPIO_Stepper.GPIO_OType	=	GPIO_OType_PP;
	GPIO_Stepper.GPIO_PuPd	= GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_Stepper);		
}

TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
TIM_OCInitTypeDef TIM_OCInitStructure;

void SERVO_PWM_Init(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
 
	TIM_TimeBaseStructure.TIM_Period=19999; //arr degeri
	TIM_TimeBaseStructure.TIM_Prescaler=84;//PrescalerValue; 84
	TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_ClockDivision=0;
 
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseStructure);
  TIM_OCInitStructure.TIM_OCMode=TIM_OCMode_PWM1;
	
	// Kanal_1
	TIM_OCInitStructure.TIM_OutputState=TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse=0;//CCR1_Val;
	TIM_OCInitStructure.TIM_OCPolarity=TIM_OCPolarity_High;
	TIM_OC1Init(TIM3,&TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM3,TIM_OCPreload_Enable); 
	
	// Kanal_2
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = 0; //CCR2_Val;
	TIM_OCInitStructure.TIM_OCPolarity=TIM_OCPolarity_High;
	TIM_OC2Init(TIM3, &TIM_OCInitStructure);
	TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);
 
	TIM_ARRPreloadConfig(TIM3,ENABLE);
	TIM_Cmd(TIM3,ENABLE);
}
 
void SERVO_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
 
	GPIO_InitStructure.GPIO_Pin= GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode= GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType= GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed= GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd= GPIO_PuPd_UP;
	GPIO_Init(GPIOC,&GPIO_InitStructure);
 
	GPIO_PinAFConfig(GPIOC,GPIO_PinSource6,GPIO_AF_TIM3);
	GPIO_PinAFConfig(GPIOC,GPIO_PinSource7,GPIO_AF_TIM3);
}

void USART_Config(void)
{
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
  
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_USART1); // Connect PB6 to USART1_T
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_USART1); // Connect PB7 to USART1_Rx

	GPIO_InitTypeDef GPIO_InitStructure;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  USART_InitTypeDef USART_InitStructure;
  USART_InitStructure.USART_BaudRate = 9600;
  USART_InitStructure.USART_HardwareFlowControl =USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART1, &USART_InitStructure);
	
  USART_Cmd(USART1, ENABLE);
}

void USART_PutChar(char c)
{
	 while (!USART_GetFlagStatus(USART1, USART_FLAG_TXE));
	 USART_SendData(USART1, c);
}

void USART_PutString(char *s)
{
	while (*s)
	{
		USART_PutChar(*s++);
	}
}

uint16_t USART_GetChar()
{
	while (!USART_GetFlagStatus(USART1, USART_FLAG_RXNE));
	return USART_ReceiveData(USART1);
}

/* Systick Handler ile bekleme olusturma */  
volatile uint32_t Delay_ms;    
void SysTick_Handler(void) 
{
	Delay_ms++;
}
 
static void Delay(__IO uint32_t bekle)
{                                              
  uint32_t t= Delay_ms;
  while ((Delay_ms - t) < bekle);
}
 
void Bekleme(void)
{
	if (SysTick_Config(SystemCoreClock / 1000)) //1ms bekleme
	{
		while (1){};
	 }
 }
//////////////////////////////////////////////
 void Delays(__IO uint32_t nCount) {
  while(nCount--) {
  }
}

char selectionStatus;
uint16_t data;
char yon='5';
int degree;

int main(void)
{
	led_config();
	stepperConfig();
	SERVO_GPIO_Init();
	SERVO_PWM_Init();
	USART_Config();
	Bekleme();
	
	USART_PutString("Test!\n");
	
	while(1)
	{
		if(USART_GetFlagStatus(USART1, USART_FLAG_RXNE))
		{
			data = USART_ReceiveData(USART1);
			yon = data;
			degree=data;
		}
		
		if(data == 'a')
		{
			selectionStatus = 'A';	// Brinci proje se�ili
		}
		else if(data == 'b')
		{
			selectionStatus = 'B';	// Ikinci proje se�ili 
		}
		else if(data == 'c')
		{
			selectionStatus = 'C';	// Ucuncu proje se�ili
		}
		else if('o')
		{
			GPIO_ResetBits(GPIOD, GPIO_Pin_All);
			GPIO_ResetBits(GPIOE, GPIO_Pin_All);
			GPIO_ResetBits(GPIOA, GPIO_Pin_All);
			GPIO_ResetBits(GPIOC, GPIO_Pin_All);
		}
		
		if(selectionStatus == 'A')
			{
				if (data == '1')
				{ 
					GPIO_ResetBits(GPIOD, GPIO_Pin_All);
					GPIO_ResetBits(GPIOE, GPIO_Pin_All);
					GPIO_SetBits(GPIOE, GPIO_Pin_14);
        }
				else if (data == '2')
				{
					GPIO_ResetBits(GPIOD, GPIO_Pin_All);
					GPIO_ResetBits(GPIOE, GPIO_Pin_All);
					GPIO_SetBits(GPIOD, GPIO_Pin_8);
        }
				else if (data == '3')
				{
					GPIO_ResetBits(GPIOD, GPIO_Pin_All);
					GPIO_ResetBits(GPIOE, GPIO_Pin_All);
					GPIO_SetBits(GPIOD, GPIO_Pin_10);
        }
				else if (data == '4')
				{
					GPIO_ResetBits(GPIOD, GPIO_Pin_All);
					GPIO_ResetBits(GPIOE, GPIO_Pin_All);
					GPIO_SetBits(GPIOD, GPIO_Pin_7);
        }
				else if (data == '5')
				{
					GPIO_ResetBits(GPIOD, GPIO_Pin_All);
					GPIO_ResetBits(GPIOE, GPIO_Pin_All);
					GPIO_SetBits(GPIOE, GPIO_Pin_10);
        }
				else if (data == '6')
				{
					GPIO_ResetBits(GPIOD, GPIO_Pin_All);
					GPIO_ResetBits(GPIOE, GPIO_Pin_All);
					GPIO_SetBits(GPIOE, GPIO_Pin_12);
        }
				else if (data == '7')
				{
					GPIO_ResetBits(GPIOD, GPIO_Pin_All);
					GPIO_ResetBits(GPIOE, GPIO_Pin_All);
					GPIO_SetBits(GPIOD, GPIO_Pin_1);
        }
				else if (data == '8')
				{
					GPIO_ResetBits(GPIOD, GPIO_Pin_All);
					GPIO_ResetBits(GPIOE, GPIO_Pin_All);
					GPIO_SetBits(GPIOD, GPIO_Pin_3);
        }
				else if (data == '9')
				{
					GPIO_ResetBits(GPIOD, GPIO_Pin_All);
					GPIO_ResetBits(GPIOE, GPIO_Pin_All);
					GPIO_SetBits(GPIOD, GPIO_Pin_5);
        }
			}
			
			else if(selectionStatus == 'B')
			{
				GPIO_ResetBits(GPIOD, GPIO_Pin_All);
				GPIO_ResetBits(GPIOE, GPIO_Pin_All);
				
				if(yon=='2')
				{
					for(int x=0 ; x<=4 ; x++)
					{
						GPIO_SetBits(GPIOA, GPIO_Pin_1);
						Delay(3);
						GPIO_SetBits(GPIOA, GPIO_Pin_3);
						Delay(3);
						GPIO_ResetBits(GPIOA, GPIO_Pin_1);
						Delay(3);
						GPIO_SetBits(GPIOA, GPIO_Pin_5);
						Delay(3);
						GPIO_ResetBits(GPIOA, GPIO_Pin_3);
						Delay(3);
						GPIO_SetBits(GPIOA, GPIO_Pin_2);
						Delay(3);
						GPIO_ResetBits(GPIOA, GPIO_Pin_5);
						Delay(3);
						GPIO_SetBits(GPIOA, GPIO_Pin_1);
						Delay(3);
						GPIO_ResetBits(GPIOA, GPIO_Pin_2);
						Delay(3);
					}
				}
				
				else if(yon=='1')
				{
					for(int x=0 ; x<=4 ; x++)
					{
						GPIO_SetBits(GPIOA, GPIO_Pin_2);
						Delay(3);
						GPIO_SetBits(GPIOA, GPIO_Pin_5);
						Delay(3);
						GPIO_ResetBits(GPIOA, GPIO_Pin_2);
						Delay(3);
						GPIO_SetBits(GPIOA, GPIO_Pin_3);
						Delay(3);
						GPIO_ResetBits(GPIOA, GPIO_Pin_5);
						Delay(3);
						GPIO_SetBits(GPIOA, GPIO_Pin_1);
						Delay(3);
						GPIO_ResetBits(GPIOA, GPIO_Pin_3);
						Delay(3);
						GPIO_SetBits(GPIOA, GPIO_Pin_2);
						Delay(3);
						GPIO_ResetBits(GPIOA, GPIO_Pin_1);
						Delay(3);
					}
				}
			}
			
			else if(selectionStatus == 'C')
			{ 
				GPIO_ResetBits(GPIOD, GPIO_Pin_All);
				GPIO_ResetBits(GPIOE, GPIO_Pin_All);
				/*if(degree!=data)
				{
					degree=data;
				}*/
				if(degree == '3')
				{
					TIM3->CCR1	= 500;  //Tim3 kesmesindeki CCR1 degerini degistir. Duty cycle
				}
				else if(degree == '2')
				{
					TIM3->CCR1	= 1550;
				}
				else if(degree == '1')
				{
					TIM3->CCR1	= 2600;
				}
				////// ikinci servo 
				/*else if(degree == '6')
				{
					TIM3->CCR2	= 710;
				}
				else if(degree == '5')
				{
					TIM3->CCR2	= 1400;
				}
				else if(degree == '4')
				{
					TIM3->CCR2	= 2200;
				}*/
			}
	}
}
