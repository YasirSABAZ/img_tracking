﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace img_Obj_Tracking
{
    
    public partial class SerialPortSetting : Form
    {
        public static string selectedPort;
        public static int selectedBaudRate;
        public static int selectedDataBits;
        public static StopBits selectedStopBits;
        public static Parity selectedParity;

        public SerialPortSetting()
        {
            InitializeComponent();
            getAvailablePorts();
        }

        void getAvailablePorts()
        {
            string[] ports = SerialPort.GetPortNames();
            selectPort.Items.AddRange(ports);
        }
        private void settingOK_Click(object sender, EventArgs e)
        {
            if (selectPort.SelectedIndex != -1 & selectBaudrate.SelectedIndex != -1 & selectParity.SelectedIndex != -1 & selectDataBits.SelectedIndex != -1 & selectStopBits.SelectedIndex != -1)
            {
                selectedPort = selectPort.Text;
                selectedBaudRate = int.Parse(selectBaudrate.Text);
                selectedDataBits = int.Parse(selectDataBits.Text);
                selectedStopBits = (StopBits)Enum.Parse(typeof(StopBits), selectStopBits.Text);
                selectedParity = (Parity)Enum.Parse(typeof(Parity), selectParity.Text);

            }
            else
            {
                MessageBox.Show("Lütfen Tüm Seri Port Ayarlarını Seçiniz !");
            }
            this.Close();
        }

        private void defaultSetting_Click(object sender, EventArgs e)
        {
            selectBaudrate.SelectedIndex = 3;
            selectDataBits.SelectedIndex = 3;
            selectStopBits.SelectedIndex = 0;
            selectParity.SelectedIndex = 0;
        }

        private void refreshPort_Click(object sender, EventArgs e)
        {
            selectPort.Items.Clear();
            string[] ports = SerialPort.GetPortNames();
            selectPort.Items.AddRange(ports);
        }
    }
}
