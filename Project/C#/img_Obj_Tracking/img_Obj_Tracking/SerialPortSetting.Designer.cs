﻿namespace img_Obj_Tracking
{
    partial class SerialPortSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.refreshPort = new System.Windows.Forms.Button();
            this.defaultSetting = new System.Windows.Forms.Button();
            this.selectParity = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.settingOK = new System.Windows.Forms.Button();
            this.selectDataBits = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.selectStopBits = new System.Windows.Forms.ComboBox();
            this.selectPort = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.selectBaudrate = new System.Windows.Forms.ComboBox();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.refreshPort);
            this.groupBox1.Controls.Add(this.defaultSetting);
            this.groupBox1.Controls.Add(this.selectParity);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.settingOK);
            this.groupBox1.Controls.Add(this.selectDataBits);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.selectStopBits);
            this.groupBox1.Controls.Add(this.selectPort);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.selectBaudrate);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(182, 192);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Port Configuration";
            // 
            // refreshPort
            // 
            this.refreshPort.Location = new System.Drawing.Point(9, 162);
            this.refreshPort.Name = "refreshPort";
            this.refreshPort.Size = new System.Drawing.Size(51, 23);
            this.refreshPort.TabIndex = 16;
            this.refreshPort.Text = "Yenile";
            this.refreshPort.UseVisualStyleBackColor = true;
            this.refreshPort.Click += new System.EventHandler(this.refreshPort_Click);
            // 
            // defaultSetting
            // 
            this.defaultSetting.Location = new System.Drawing.Point(66, 162);
            this.defaultSetting.Name = "defaultSetting";
            this.defaultSetting.Size = new System.Drawing.Size(51, 23);
            this.defaultSetting.TabIndex = 15;
            this.defaultSetting.Text = "Default";
            this.defaultSetting.UseVisualStyleBackColor = true;
            this.defaultSetting.Click += new System.EventHandler(this.defaultSetting_Click);
            // 
            // selectParity
            // 
            this.selectParity.FormattingEnabled = true;
            this.selectParity.Items.AddRange(new object[] {
            "None",
            "Even"});
            this.selectParity.Location = new System.Drawing.Point(64, 137);
            this.selectParity.Name = "selectParity";
            this.selectParity.Size = new System.Drawing.Size(80, 21);
            this.selectParity.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Parity";
            // 
            // settingOK
            // 
            this.settingOK.Location = new System.Drawing.Point(123, 163);
            this.settingOK.Name = "settingOK";
            this.settingOK.Size = new System.Drawing.Size(51, 23);
            this.settingOK.TabIndex = 9;
            this.settingOK.Text = "Tamam";
            this.settingOK.UseVisualStyleBackColor = true;
            this.settingOK.Click += new System.EventHandler(this.settingOK_Click);
            // 
            // selectDataBits
            // 
            this.selectDataBits.FormattingEnabled = true;
            this.selectDataBits.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.selectDataBits.Location = new System.Drawing.Point(64, 78);
            this.selectDataBits.Name = "selectDataBits";
            this.selectDataBits.Size = new System.Drawing.Size(80, 21);
            this.selectDataBits.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Stop Bits";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Data Bits";
            // 
            // selectStopBits
            // 
            this.selectStopBits.FormattingEnabled = true;
            this.selectStopBits.Items.AddRange(new object[] {
            "1",
            "2"});
            this.selectStopBits.Location = new System.Drawing.Point(64, 110);
            this.selectStopBits.Name = "selectStopBits";
            this.selectStopBits.Size = new System.Drawing.Size(80, 21);
            this.selectStopBits.TabIndex = 10;
            // 
            // selectPort
            // 
            this.selectPort.FormattingEnabled = true;
            this.selectPort.Location = new System.Drawing.Point(64, 19);
            this.selectPort.Name = "selectPort";
            this.selectPort.Size = new System.Drawing.Size(80, 21);
            this.selectPort.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Baud Rate";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Com Port";
            // 
            // selectBaudrate
            // 
            this.selectBaudrate.FormattingEnabled = true;
            this.selectBaudrate.Items.AddRange(new object[] {
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "115200"});
            this.selectBaudrate.Location = new System.Drawing.Point(64, 51);
            this.selectBaudrate.Name = "selectBaudrate";
            this.selectBaudrate.Size = new System.Drawing.Size(80, 21);
            this.selectBaudrate.TabIndex = 6;
            // 
            // SerialPortSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(202, 213);
            this.Controls.Add(this.groupBox1);
            this.Name = "SerialPortSetting";
            this.Text = "SerialPortSetting";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox selectParity;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button settingOK;
        private System.Windows.Forms.ComboBox selectDataBits;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox selectStopBits;
        private System.Windows.Forms.ComboBox selectPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox selectBaudrate;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Button refreshPort;
        private System.Windows.Forms.Button defaultSetting;
    }
}