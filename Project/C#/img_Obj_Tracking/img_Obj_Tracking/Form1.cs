﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge;
using AForge.Imaging;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Imaging.Filters;
using System.Drawing.Imaging;
using System.IO.Ports;
using System.Threading;

namespace img_Obj_Tracking
{

    public partial class Form1 : Form
    {
        
        private FilterInfoCollection videoCameras;
        private VideoCaptureDevice Cam;
        int R = 0, G = 0, B = 0;
        int nesneX, nesneY, nesne2X, nesne2Y;
        int led;
        int ledStatus=0;
        string ledYak;
        int stepRotation;
        int stepStatus=4;
        string step;
        int projectCheck = 1;
        int temp = 0;
        string servoStatus1;
        string servoStatus2;
        public Form1()
        {
            InitializeComponent();
            stop.Enabled = false;
            capture.Enabled = false;
            settingPort.Enabled = false;
            videoCameras = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if(videoCameras.Count != 0)
            {
                for(int i=1; i<= videoCameras.Count; i++)
                {
                    string videoCameraName = i + ":" + videoCameras[i - 1].Name;
                    cameraDetect.Items.Add(videoCameraName);
                }
                if(videoCameras.Count == 1)
                {
                    cameraDetect.SelectedIndex = 0;
                    cameraDetect.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show("Görüntü Sağlayıcı Kamera Bulunamadı !");
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            R = red.Value;
            redValue.Text = Convert.ToString("R: "+R);
        }

        private void green_Scroll(object sender, EventArgs e)
        {
            G = green.Value;
            greenValue.Text = Convert.ToString("G: " + G);
        }

        private void blue_Scroll(object sender, EventArgs e)
        {
            B = blue.Value;
            blueValue.Text = Convert.ToString("B: " + B);
        }

        private void stop_Click(object sender, EventArgs e)
        {
            start.Enabled = true;
            stop.Enabled = false;
            if (Cam.IsRunning)
            {
                Cam.Stop();
            }
        }

        private void capture_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "PNG|*.png";
            DialogResult result = saveFileDialog1.ShowDialog();
            ImageFormat format = ImageFormat.Png;

            if(result == DialogResult.OK)
            {
                view.Image.Save(saveFileDialog1.FileName, format);
            }
        }

        private void settingPort_Click(object sender, EventArgs e)
        {
            SerialPortSetting setupCom = new SerialPortSetting();
            setupCom.Show();
            toogleConnect.Enabled = true;
        }

        private SerialPort comPort = new SerialPort();

        private void comSetup()
        {
            bool error = false;
            string selectedPortName = SerialPortSetting.selectedPort;
            int selectedBaudRateValue = SerialPortSetting.selectedBaudRate;
            int selectedDataBitsValue = SerialPortSetting.selectedDataBits;
            StopBits selectedStopBitsValue = SerialPortSetting.selectedStopBits;
            Parity selectedParityValue = SerialPortSetting.selectedParity;
            if (selectedPortName != null)
            {
                comPort.PortName = selectedPortName;
                comPort.BaudRate = selectedBaudRateValue;
                comPort.DataBits = selectedDataBitsValue;
                comPort.StopBits = selectedStopBitsValue;
                comPort.Parity = selectedParityValue;
            }
            else
            {
                MessageBox.Show("Eksik seri port bağlantı ayarı tespit edildi.");
            }
           
            try
            {
                comPort.Open();
            }

            catch (UnauthorizedAccessException) { error = true; }
            catch (System.IO.IOException) { error = true; }
            catch (ArgumentException) { error = true; }

            if (error) MessageBox.Show(this, "COM PORT Açılamadı.Büyük olasılıkla kullanımda, kaldırılmış veya mevcut değil", "COM PORT Kullanım dışı", MessageBoxButtons.OK, MessageBoxIcon.Stop);

        }
        private void toogleConnect_Click(object sender, EventArgs e)
        {
            string selectedPortName = SerialPortSetting.selectedPort;
            if (selectedPortName != null)
            {
                if (comPort.IsOpen)
                {
                    comPort.Close();
                    toogleConnect.Text = "Bağlan";
                    settingPort.Enabled = true;
                    
                }
                else
                {
                    comSetup();
                   // comPort.Write("1");
                    toogleConnect.Text = "Bağlantıyı Kes";
                    settingPort.Enabled = false;
                    
                }
            } 
                
            

            
        }

        private void ledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            projectCheck = 1;
            projectView.Text = "1";
            ledToolStripMenuItem.Checked = true;
            stepToolStripMenuItem.Checked = false;
            servoToolStripMenuItem.Checked = false;
            coor2List.Visible = false;
            nesne2Coor.Visible = false;
            if(comPort.IsOpen)
            {
                comPort.Write("a");
            }
        }

        private void stepToolStripMenuItem_Click(object sender, EventArgs e)
        {
            projectCheck = 2;
            projectView.Text = "2";
            ledToolStripMenuItem.Checked = false;
            stepToolStripMenuItem.Checked = true;
            servoToolStripMenuItem.Checked = false;
            coor2List.Visible = false;
            nesne2Coor.Visible = false;
            if (comPort.IsOpen)
            {
                comPort.Write("b");
            }
        }

        private void emergencyStop_Click(object sender, EventArgs e)
        {
            projectCheck = 0;
            if (comPort.IsOpen)
            {
                comPort.Write("o");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            toogleConnect.Enabled = false;
        }

       
        private void catchCoor_Click(object sender, EventArgs e)
        {
            if (!colorCatch.Checked)
            {
                MessageBox.Show("Hedeflenen Bir Cisim Yok!");
                catchCoor.Checked = false;
            }
            else if (!comPort.IsOpen)
            {
                MessageBox.Show("Seri Portu Açınız!");
                catchCoor.Checked = false;
            }
        }

        private void servoMotorİeTakipToolStripMenuItem_Click(object sender, EventArgs e)
        {
            projectCheck = 3;
            projectView.Text = "3";
            ledToolStripMenuItem.Checked = false;
            stepToolStripMenuItem.Checked = false;
            servoToolStripMenuItem.Checked = true;
            coor2List.Visible = true;
            nesne2Coor.Visible = true;
            if (comPort.IsOpen)
            {
                comPort.Write("c");
            }

        }


        private void start_Click(object sender, EventArgs e)
        {

            projectCheck = 1;
            projectView.Text = "1";
            ledToolStripMenuItem.Checked = true;
            stepToolStripMenuItem.Checked = false;
            servoToolStripMenuItem.Checked = false;
            coor2List.Visible = false;
            nesne2Coor.Visible = false;

            Cam = new VideoCaptureDevice(videoCameras[cameraDetect.SelectedIndex].MonikerString);
            Cam.NewFrame += new NewFrameEventHandler(camNewFrame);
            //Cam.DesiredFrameRate = 30;
            //Cam.DesiredFrameSize = new Size(320,250);
            Cam.Start();
            stop.Enabled = true;
            start.Enabled = false;
            capture.Enabled = true;
            settingPort.Enabled = true;
        }
        void camNewFrame(object sender, NewFrameEventArgs main)
        {
            Bitmap sourceImg = (Bitmap)main.Frame.Clone();
            Bitmap processImg = (Bitmap)main.Frame.Clone();
            //ResizeBicubic resizeFilter = new ResizeBicubic(640, 480);
            //sourceImg=resizeFilter.Apply(sourceImg);
            //processImg=resizeFilter.Apply(processImg);
            Mirror filter = new Mirror(false, true);
            filter.ApplyInPlace(sourceImg);
            filter.ApplyInPlace(processImg);
            cameraSource.Image= sourceImg;

            if(greyFilter.Checked)
            {
                processImg = new GrayscaleBT709().Apply(processImg);
            }
            else if(otsuFilter.Checked)
            {
                processImg = new GrayscaleBT709().Apply(processImg);
                processImg = new OtsuThreshold().Apply(processImg);
            }
            else if(colorCatch.Checked)
            {
                EuclideanColorFiltering colorFilter = new EuclideanColorFiltering();
                colorFilter.CenterColor = new RGB(Color.FromArgb(R, G, B));
                colorFilter.Radius = 100;
                colorFilter.ApplyInPlace(processImg);
                BlobCounter blobCounter = new BlobCounter();
                blobCounter.MinHeight = 20;
                blobCounter.MinWidth = 20;
                blobCounter.ObjectsOrder = ObjectsOrder.Size;
                blobCounter.ProcessImage(processImg);
                Rectangle[] rectangles = blobCounter.GetObjectsRectangles();

                if (projectCheck == 1)
                {
                    if (rectangles.Length > 0)
                    {
                        Rectangle nesne = rectangles[0];
                        Graphics graphics = Graphics.FromImage(processImg);
                        using (Pen penFrame = new Pen(Color.White, 1))
                        {
                            graphics.DrawRectangle(penFrame, nesne);
                        }
                        graphics.Dispose();
                        nesneX = nesne.X + (nesne.Width / 2);
                        nesneY = nesne.Y + (nesne.Height / 2);

                        if (catchCoor.Checked)
                        {
                            this.Invoke((MethodInvoker)delegate
                            {
                                coorList.Text = "x: " + nesneX.ToString() + "--Y: " + nesneY.ToString() + "\n" + coorList.Text + "\n";
                            });
                            if (nesneX >= 0 && nesneX <= 213 && nesneY >= 0 && nesneY <= 160)
                            {
                                led = 1;
                            }
                            else if (nesneX >= 214 && nesneX <= 426 && nesneY >= 0 && nesneY <= 160)
                            {
                                led = 2;
                            }
                            else if (nesneX >= 427 && nesneX <= 640 && nesneY >= 0 && nesneY <= 160)
                            {
                                led = 3;
                            }
                            else if (nesneX >= 0 && nesneX <= 213 && nesneY >= 160 && nesneY <= 320)
                            {
                                led = 4;
                            }
                            else if (nesneX >= 214 && nesneX <= 426 && nesneY >= 160 && nesneY <= 320)
                            {
                                led = 5;
                            }
                            else if (nesneX >= 427 && nesneX <= 640 && nesneY >= 160 && nesneY <= 320)
                            {
                                led = 6;
                            }
                            else if (nesneX >= 0 && nesneX <= 213 && nesneY >= 320 && nesneY <= 480)
                            {
                                led = 7;
                            }
                            else if (nesneX >= 214 && nesneX <= 426 && nesneY >= 320 && nesneY <= 480)
                            {
                                led = 8;
                            }
                            else if (nesneX >= 427 && nesneX <= 640 && nesneY >= 320 && nesneY <= 480)
                            {
                                led = 9;
                            }

                            ledYak = Convert.ToString(led);

                            if (comPort.IsOpen)
                            {
                                /*if (temp != 1)
                                {
                                    comPort.Write("a");
                                    temp = 1;
                                }*/
                                comPort.Write(ledYak);
                            }
                        }
                    }
                }

                else if (projectCheck == 2)
                {
                    if (rectangles.Length > 0)
                    {
                        Rectangle nesne = rectangles[0];
                        Graphics graphics = Graphics.FromImage(processImg);
                        using (Pen penFrame = new Pen(Color.White, 1))
                        {
                            graphics.DrawRectangle(penFrame, nesne);
                        }
                        graphics.Dispose();
                        nesneX = nesne.X + (nesne.Width / 2);
                        nesneY = nesne.Y + (nesne.Height / 2);

                        if (catchCoor.Checked)
                        {
                            this.Invoke((MethodInvoker)delegate
                            {
                                coorList.Text = "x: " + nesneX.ToString() + "--Y: " + nesneY.ToString() + "\n" + coorList.Text + "\n";
                            });

                            if (nesneX >= 280 && nesneX <= 400)
                            {
                                stepRotation = 0; //sabit
                            }
                            else if(nesneX >= 0 && nesneX <= 280)
                            {
                                stepRotation = 1; //CCW
                            }
                            else if (nesneX >= 400 && nesneX <= 680)
                            {
                                stepRotation = 2; //CW
                            }
                            step = Convert.ToString(stepRotation);
                            if (comPort.IsOpen)
                            {
                                /*if (temp != 1)
                                {
                                    comPort.Write("b");
                                    temp = 1;
                                }*/
                                comPort.Write(step);
                            }
                        }
                    }
                
                    else
                    {
                        if (comPort.IsOpen)
                        {
                            comPort.Write("0");
                        }
                    }
                }

                else if (projectCheck == 3)
                {
                    if (rectangles.Length > 1)
                    {
                        for (int i = 0; i < 2; i++)
                        {
                            Rectangle nesne = rectangles[i];
                            Graphics graphics = Graphics.FromImage(processImg);
                            using (Pen penFrame = new Pen(Color.White, 1))
                            {
                                graphics.DrawRectangle(penFrame, nesne);
                                graphics.DrawString((i + 1).ToString(), new Font("Arial", 12), Brushes.White, nesne);
                            }
                            graphics.Dispose();

                            if (rectangles.Length >= 1)
                            {
                                Rectangle nesne1 = rectangles[0];
                                Rectangle nesne2 = rectangles[1];

                                nesneX = nesne.X + (nesne.Width / 2);
                                nesneY = nesne.Y + (nesne.Height / 2);

                                nesne2X = nesne2.X + (nesne.Width / 2);
                                nesne2Y = nesne2.Y + (nesne.Height / 2);

                                if (catchCoor.Checked)
                                {
                                    this.Invoke((MethodInvoker)delegate
                                    {
                                        coorList.Text = "x: " + nesneX.ToString() + "--Y: " + nesneY.ToString() + "\n" + coorList.Text + "\n";
                                        coor2List.Text = "x: " + nesne2X.ToString() + "--Y: " + nesne2Y.ToString() + "\n" + coor2List.Text + "\n";
                                    });

                                    if (nesneY >= 0 && nesneY <= 160)
                                    {
                                        servoStatus1 = "3";
                                    }
                                    else if (nesneY > 160 && nesneY <= 320)
                                    {
                                        servoStatus1 = "2";
                                    }
                                    else if (nesneY > 320 && nesneY <= 480)
                                    {
                                        servoStatus1 = "1";
                                    }

                                    if (comPort.IsOpen)
                                    {
                                        comPort.Write(servoStatus1);
                                    }

                                    if (nesne2Y >= 0 && nesne2Y <= 160)
                                    {
                                        servoStatus2 = "6";
                                    }
                                    else if (nesne2Y > 160 && nesne2Y <= 320)
                                    {
                                        servoStatus2 = "5";
                                    }
                                    else if (nesne2Y > 320 && nesne2Y <= 480)
                                    {
                                        servoStatus2 = "4";
                                    }

                                    if (comPort.IsOpen)
                                    {
                                        /*if (temp != 1)
                                        {
                                            comPort.Write("c");
                                            temp = 1;
                                        }*/
                                        comPort.Write(servoStatus2);
                                    }
                                }
                            }
                        }
                    }
                }
          
            }
            view.Image = processImg;
        }
    }
}
