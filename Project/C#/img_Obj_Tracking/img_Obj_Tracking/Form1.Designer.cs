﻿namespace img_Obj_Tracking
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dosyaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.işlemlerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ledToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stepToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.servoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cameraSource = new System.Windows.Forms.PictureBox();
            this.view = new System.Windows.Forms.PictureBox();
            this.start = new System.Windows.Forms.Button();
            this.stop = new System.Windows.Forms.Button();
            this.capture = new System.Windows.Forms.Button();
            this.red = new System.Windows.Forms.TrackBar();
            this.green = new System.Windows.Forms.TrackBar();
            this.blue = new System.Windows.Forms.TrackBar();
            this.redValue = new System.Windows.Forms.Label();
            this.greenValue = new System.Windows.Forms.Label();
            this.blueValue = new System.Windows.Forms.Label();
            this.cameraDetect = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.settingPort = new System.Windows.Forms.Button();
            this.toogleConnect = new System.Windows.Forms.Button();
            this.otsuFilter = new System.Windows.Forms.RadioButton();
            this.greyFilter = new System.Windows.Forms.RadioButton();
            this.colorCatch = new System.Windows.Forms.RadioButton();
            this.catchCoor = new System.Windows.Forms.CheckBox();
            this.coorList = new System.Windows.Forms.RichTextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.projectView = new System.Windows.Forms.Label();
            this.coor2List = new System.Windows.Forms.RichTextBox();
            this.nesne2Coor = new System.Windows.Forms.Label();
            this.nesne1Coor = new System.Windows.Forms.Label();
            this.emergencyStop = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.view)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.red)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.green)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blue)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dosyaToolStripMenuItem,
            this.işlemlerToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(803, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dosyaToolStripMenuItem
            // 
            this.dosyaToolStripMenuItem.Name = "dosyaToolStripMenuItem";
            this.dosyaToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.dosyaToolStripMenuItem.Text = "Dosya";
            // 
            // işlemlerToolStripMenuItem
            // 
            this.işlemlerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ledToolStripMenuItem,
            this.stepToolStripMenuItem,
            this.servoToolStripMenuItem});
            this.işlemlerToolStripMenuItem.Name = "işlemlerToolStripMenuItem";
            this.işlemlerToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.işlemlerToolStripMenuItem.Text = "İşlemler";
            // 
            // ledToolStripMenuItem
            // 
            this.ledToolStripMenuItem.Name = "ledToolStripMenuItem";
            this.ledToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.ledToolStripMenuItem.Text = "Led İle Takip";
            this.ledToolStripMenuItem.Click += new System.EventHandler(this.ledToolStripMenuItem_Click);
            // 
            // stepToolStripMenuItem
            // 
            this.stepToolStripMenuItem.Name = "stepToolStripMenuItem";
            this.stepToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.stepToolStripMenuItem.Text = "Step Motor İle Takip";
            this.stepToolStripMenuItem.Click += new System.EventHandler(this.stepToolStripMenuItem_Click);
            // 
            // servoToolStripMenuItem
            // 
            this.servoToolStripMenuItem.Name = "servoToolStripMenuItem";
            this.servoToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.servoToolStripMenuItem.Text = "Servo Motor İe Takip";
            this.servoToolStripMenuItem.Click += new System.EventHandler(this.servoMotorİeTakipToolStripMenuItem_Click);
            // 
            // cameraSource
            // 
            this.cameraSource.Location = new System.Drawing.Point(12, 27);
            this.cameraSource.Name = "cameraSource";
            this.cameraSource.Size = new System.Drawing.Size(320, 250);
            this.cameraSource.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cameraSource.TabIndex = 1;
            this.cameraSource.TabStop = false;
            // 
            // view
            // 
            this.view.Location = new System.Drawing.Point(475, 27);
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(320, 250);
            this.view.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.view.TabIndex = 2;
            this.view.TabStop = false;
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(338, 47);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(66, 23);
            this.start.TabIndex = 3;
            this.start.Text = "Başlat";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // stop
            // 
            this.stop.Location = new System.Drawing.Point(403, 47);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(66, 23);
            this.stop.TabIndex = 4;
            this.stop.Text = "Durdur";
            this.stop.UseVisualStyleBackColor = true;
            this.stop.Click += new System.EventHandler(this.stop_Click);
            // 
            // capture
            // 
            this.capture.Location = new System.Drawing.Point(338, 70);
            this.capture.Name = "capture";
            this.capture.Size = new System.Drawing.Size(131, 23);
            this.capture.TabIndex = 5;
            this.capture.Text = "Ekranı Yakala";
            this.capture.UseVisualStyleBackColor = true;
            this.capture.Click += new System.EventHandler(this.capture_Click);
            // 
            // red
            // 
            this.red.BackColor = System.Drawing.SystemColors.Control;
            this.red.Cursor = System.Windows.Forms.Cursors.Hand;
            this.red.Location = new System.Drawing.Point(338, 99);
            this.red.Maximum = 255;
            this.red.Name = "red";
            this.red.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.red.Size = new System.Drawing.Size(45, 194);
            this.red.TabIndex = 6;
            this.red.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // green
            // 
            this.green.BackColor = System.Drawing.SystemColors.Control;
            this.green.Cursor = System.Windows.Forms.Cursors.Hand;
            this.green.Location = new System.Drawing.Point(380, 99);
            this.green.Maximum = 255;
            this.green.Name = "green";
            this.green.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.green.Size = new System.Drawing.Size(45, 194);
            this.green.TabIndex = 7;
            this.green.Scroll += new System.EventHandler(this.green_Scroll);
            // 
            // blue
            // 
            this.blue.BackColor = System.Drawing.SystemColors.Control;
            this.blue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.blue.Location = new System.Drawing.Point(424, 99);
            this.blue.Maximum = 255;
            this.blue.Name = "blue";
            this.blue.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.blue.Size = new System.Drawing.Size(45, 194);
            this.blue.TabIndex = 8;
            this.blue.Scroll += new System.EventHandler(this.blue_Scroll);
            // 
            // redValue
            // 
            this.redValue.AutoSize = true;
            this.redValue.Location = new System.Drawing.Point(335, 290);
            this.redValue.Name = "redValue";
            this.redValue.Size = new System.Drawing.Size(15, 13);
            this.redValue.TabIndex = 9;
            this.redValue.Text = "R";
            // 
            // greenValue
            // 
            this.greenValue.AutoSize = true;
            this.greenValue.Location = new System.Drawing.Point(379, 290);
            this.greenValue.Name = "greenValue";
            this.greenValue.Size = new System.Drawing.Size(15, 13);
            this.greenValue.TabIndex = 10;
            this.greenValue.Text = "G";
            // 
            // blueValue
            // 
            this.blueValue.AutoSize = true;
            this.blueValue.Location = new System.Drawing.Point(422, 290);
            this.blueValue.Name = "blueValue";
            this.blueValue.Size = new System.Drawing.Size(14, 13);
            this.blueValue.TabIndex = 11;
            this.blueValue.Text = "B";
            // 
            // cameraDetect
            // 
            this.cameraDetect.FormattingEnabled = true;
            this.cameraDetect.Location = new System.Drawing.Point(338, 27);
            this.cameraDetect.Name = "cameraDetect";
            this.cameraDetect.Size = new System.Drawing.Size(131, 21);
            this.cameraDetect.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.settingPort);
            this.groupBox1.Controls.Add(this.toogleConnect);
            this.groupBox1.Location = new System.Drawing.Point(475, 283);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox1.Size = new System.Drawing.Size(320, 59);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seri Port";
            // 
            // settingPort
            // 
            this.settingPort.Location = new System.Drawing.Point(167, 21);
            this.settingPort.Name = "settingPort";
            this.settingPort.Size = new System.Drawing.Size(147, 23);
            this.settingPort.TabIndex = 1;
            this.settingPort.Text = "Ayarlar";
            this.settingPort.UseVisualStyleBackColor = true;
            this.settingPort.Click += new System.EventHandler(this.settingPort_Click);
            // 
            // toogleConnect
            // 
            this.toogleConnect.Location = new System.Drawing.Point(6, 23);
            this.toogleConnect.Name = "toogleConnect";
            this.toogleConnect.Size = new System.Drawing.Size(155, 23);
            this.toogleConnect.TabIndex = 0;
            this.toogleConnect.Text = "Bağlan";
            this.toogleConnect.UseVisualStyleBackColor = true;
            this.toogleConnect.Click += new System.EventHandler(this.toogleConnect_Click);
            // 
            // otsuFilter
            // 
            this.otsuFilter.AutoSize = true;
            this.otsuFilter.Location = new System.Drawing.Point(110, 286);
            this.otsuFilter.Name = "otsuFilter";
            this.otsuFilter.Size = new System.Drawing.Size(47, 17);
            this.otsuFilter.TabIndex = 17;
            this.otsuFilter.TabStop = true;
            this.otsuFilter.Text = "Otsu";
            this.otsuFilter.UseVisualStyleBackColor = true;
            // 
            // greyFilter
            // 
            this.greyFilter.AutoSize = true;
            this.greyFilter.Location = new System.Drawing.Point(163, 286);
            this.greyFilter.Name = "greyFilter";
            this.greyFilter.Size = new System.Drawing.Size(74, 17);
            this.greyFilter.TabIndex = 18;
            this.greyFilter.TabStop = true;
            this.greyFilter.Text = "GrayScale";
            this.greyFilter.UseVisualStyleBackColor = true;
            // 
            // colorCatch
            // 
            this.colorCatch.AutoSize = true;
            this.colorCatch.Location = new System.Drawing.Point(243, 286);
            this.colorCatch.Name = "colorCatch";
            this.colorCatch.Size = new System.Drawing.Size(87, 17);
            this.colorCatch.TabIndex = 19;
            this.colorCatch.TabStop = true;
            this.colorCatch.Text = "Renk Yakala";
            this.colorCatch.UseVisualStyleBackColor = true;
            // 
            // catchCoor
            // 
            this.catchCoor.AutoSize = true;
            this.catchCoor.Location = new System.Drawing.Point(12, 286);
            this.catchCoor.Name = "catchCoor";
            this.catchCoor.Size = new System.Drawing.Size(96, 17);
            this.catchCoor.TabIndex = 21;
            this.catchCoor.Text = "Kordinat Belirle";
            this.catchCoor.UseVisualStyleBackColor = true;
            this.catchCoor.Click += new System.EventHandler(this.catchCoor_Click);
            // 
            // coorList
            // 
            this.coorList.Location = new System.Drawing.Point(75, 314);
            this.coorList.Name = "coorList";
            this.coorList.Size = new System.Drawing.Size(118, 20);
            this.coorList.TabIndex = 22;
            this.coorList.Text = "";
            // 
            // projectView
            // 
            this.projectView.AutoSize = true;
            this.projectView.BackColor = System.Drawing.Color.Red;
            this.projectView.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.projectView.ForeColor = System.Drawing.SystemColors.ControlText;
            this.projectView.Location = new System.Drawing.Point(755, 6);
            this.projectView.Name = "projectView";
            this.projectView.Size = new System.Drawing.Size(36, 13);
            this.projectView.TabIndex = 23;
            this.projectView.Text = "Seçim";
            // 
            // coor2List
            // 
            this.coor2List.Location = new System.Drawing.Point(264, 312);
            this.coor2List.Name = "coor2List";
            this.coor2List.Size = new System.Drawing.Size(118, 20);
            this.coor2List.TabIndex = 24;
            this.coor2List.Text = "";
            // 
            // nesne2Coor
            // 
            this.nesne2Coor.AutoSize = true;
            this.nesne2Coor.Location = new System.Drawing.Point(204, 318);
            this.nesne2Coor.Name = "nesne2Coor";
            this.nesne2Coor.Size = new System.Drawing.Size(55, 13);
            this.nesne2Coor.TabIndex = 25;
            this.nesne2Coor.Text = "Kordinat 2";
            // 
            // nesne1Coor
            // 
            this.nesne1Coor.AutoSize = true;
            this.nesne1Coor.Location = new System.Drawing.Point(15, 319);
            this.nesne1Coor.Name = "nesne1Coor";
            this.nesne1Coor.Size = new System.Drawing.Size(55, 13);
            this.nesne1Coor.TabIndex = 26;
            this.nesne1Coor.Text = "Kordinat 1";
            // 
            // emergencyStop
            // 
            this.emergencyStop.Location = new System.Drawing.Point(395, 307);
            this.emergencyStop.Name = "emergencyStop";
            this.emergencyStop.Size = new System.Drawing.Size(80, 23);
            this.emergencyStop.TabIndex = 27;
            this.emergencyStop.Text = "A.STOP";
            this.emergencyStop.UseVisualStyleBackColor = true;
            this.emergencyStop.Click += new System.EventHandler(this.emergencyStop_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 342);
            this.Controls.Add(this.emergencyStop);
            this.Controls.Add(this.nesne1Coor);
            this.Controls.Add(this.nesne2Coor);
            this.Controls.Add(this.coor2List);
            this.Controls.Add(this.projectView);
            this.Controls.Add(this.coorList);
            this.Controls.Add(this.catchCoor);
            this.Controls.Add(this.colorCatch);
            this.Controls.Add(this.greyFilter);
            this.Controls.Add(this.otsuFilter);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cameraDetect);
            this.Controls.Add(this.blueValue);
            this.Controls.Add(this.greenValue);
            this.Controls.Add(this.redValue);
            this.Controls.Add(this.blue);
            this.Controls.Add(this.green);
            this.Controls.Add(this.red);
            this.Controls.Add(this.capture);
            this.Controls.Add(this.stop);
            this.Controls.Add(this.start);
            this.Controls.Add(this.view);
            this.Controls.Add(this.cameraSource);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.view)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.red)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.green)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blue)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dosyaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem işlemlerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ledToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stepToolStripMenuItem;
        private System.Windows.Forms.Label greenValue;
        private System.Windows.Forms.Label redValue;
        private System.Windows.Forms.TrackBar blue;
        private System.Windows.Forms.TrackBar green;
        private System.Windows.Forms.TrackBar red;
        private System.Windows.Forms.Button capture;
        private System.Windows.Forms.Button stop;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.PictureBox view;
        private System.Windows.Forms.PictureBox cameraSource;
        private System.Windows.Forms.Label blueValue;
        private System.Windows.Forms.RadioButton colorCatch;
        private System.Windows.Forms.RadioButton greyFilter;
        private System.Windows.Forms.RadioButton otsuFilter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cameraDetect;
        private System.Windows.Forms.CheckBox catchCoor;
        private System.Windows.Forms.RichTextBox coorList;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button settingPort;
        private System.Windows.Forms.Button toogleConnect;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.ToolStripMenuItem servoToolStripMenuItem;
        private System.Windows.Forms.Label projectView;
        private System.Windows.Forms.RichTextBox coor2List;
        private System.Windows.Forms.Label nesne2Coor;
        private System.Windows.Forms.Label nesne1Coor;
        private System.Windows.Forms.Button emergencyStop;
    }
}

